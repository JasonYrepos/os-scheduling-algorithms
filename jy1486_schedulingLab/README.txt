Jason Yang Lab 2

Scheduling Lab (Lab 2) for Operating Systems Spring 2016
1 source code file(s) are part of this submission:
        -> scheduling.cpp


To compile the lab, execute:
$ g++ -std=gnu++0x scheduling.cpp



To run the lab, execute:
$ ./a.out --verbose <input-filename>
or
$ ./a.out <input-filename>

gets random numbers from "Random Number.txt" in the same file location as the source.

The code was tested on mauler.cims.nyu.edu.



C++

