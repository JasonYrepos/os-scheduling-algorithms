#include <iostream>
#include <math.h>
#include <algorithm>
#include <iomanip>
#include <vector>
#include <fstream>
#include <string>
#include <deque>
using namespace std;


//Jason Yang Lab2
class process;
int randomOS(int U);
bool floatequal(double a1, double b1);
process getfirst(vector<process> processlistt,int istate);
vector<process> rearrange(vector<process>*);
void FCFS(vector<process> , int);
void HPRN(vector<process> , int);
void RR2(vector<process> , int);
void LCFS(vector<process> ,int);
vector<int> numba;
vector<string> lettr;
vector<int> pnumbers;
int rcount = 0;
int totnu;

vector<process> plist;

class process{
public:
    int arrivet;
    int cpuburst ;
    int remaint ;
    int ioburst ;
    int finish ;
    double turntime ;
    double iotime ;
    double waittime ;

    int opriority  ;
    int cburst; // constant
    int oburst; // constant
    int priority;
    float ncycle;
    int totcputime; //constant
process(int rrr, int ccc, int mmm, int iii ){
opriority  = -1;
    cpuburst = 0;

     ioburst = 0;
    finish = 0;
   turntime = 0;
  iotime = 0;
    waittime = 0;
    arrivet = rrr;
    remaint = mmm;
    cburst = ccc;
    oburst = iii;
    totcputime = mmm;

}
double getratio() const{
    if((totcputime - remaint)==0){
        return floorf((double)((ncycle - arrivet) * 10000))/10000;
    }else{
        return floorf((double)((ncycle - arrivet)/(totcputime-remaint)* 10000))/10000;
    }
}
bool operator==( const process& anthhr){
    if(priority == anthhr.priority){
        return true;
    }
    return false;
}
friend class hprnfunctor;
friend class arrivalfunctor;
friend class priorityfunctor;
friend class antipriorityfunctor;
    class antipriorityfunctor{
    public:
        bool operator()(const process& pr1,const process& pr2){
        if(pr1.priority < pr2.priority){

          return false;
        }else {
        return true;

        }

        }
    };

    class priorityfunctor{
    public:
        bool operator()(const process& pr1,const process& pr2){
        if(pr1.priority > pr2.priority){

          return false;
        }else {
        return true;

        }

        }
    };

    class arrivalfunctor{
    public:
        bool operator()(const process& pr1,const process& pr2){
        if(pr1.arrivet > pr2.arrivet){

          return false;
        }else if(pr1.arrivet < pr2.arrivet){

        return true;
        }
        else if (pr1.arrivet == pr2.arrivet){
            if(pr1.opriority > pr2.opriority ){

                return false;

            }else{
           return true;

            }
        }
        }
    };

    class hprnfunctor {
        public:
             bool operator()(const process& pr1,const process& pr2){


              if (floatequal(pr1.getratio(),pr2.getratio())){
                if(pr1.priority < pr2.priority){

                    return true;
                }else{

            return false;
                }
            }else{


        if(pr1.getratio() > pr2.getratio()){

       return true;
            }else if(pr1.getratio() < pr2.getratio()){
                return false;

            }



            }



             }

    };
void coutproc(){
cout << arrivet << " " << cburst << " " << remaint << " " << oburst << " ";
}


};

int main(int argc, char* argv[]){
int verb = 0;
string voption;
voption = argv[1];
if(voption == "--verbose"){
    verb = 1;
}
string filename;
if(argc == 2){
    filename = argv[1];
}else if(argc == 3){
    filename = argv[2];
}
ifstream rfile;
ifstream lfile;
 pnumbers;
int pnumber;
int numbers;


rfile.open("./Random Number.txt");
while(!rfile.eof()){
    rfile >> numbers;
    numba.push_back(numbers);

}

rfile.close();
lfile.open(filename);
while(!lfile.eof()){
   lfile >> pnumber;
   pnumbers.push_back(pnumber);

}
lfile.close();
 totnu = pnumbers[0];


for(int i = 1; i < (totnu * 4) ; i= i + 4){
    plist.push_back(process(pnumbers[i],pnumbers[i+1],pnumbers[i+2],pnumbers[i+3]));
}



FCFS(plist,verb);
cout << "\n";
RR2(plist,verb);
cout << "\n";
LCFS(plist,verb);
cout << "\n";
HPRN(plist,verb);



}
void LCFS(vector<process> myproc, int verbose){
int cycle = 0;
int CPUtime = 0;
int iotime = 0;
int finishproc = 0;
process *curproc = NULL;


process theproc(1,1,1,1);
deque<process> rdy;
vector<process> block;
vector<process> finished;
vector<process> notstart;


cout << "The original input was: " << totnu << "   ";

for(int i = 1; i < pnumbers.size() - 1; i = i+4){
 cout << pnumbers[i] << " ";
 cout << pnumbers[i+1] << " ";
 cout << pnumbers[i+2] << " ";
 cout << pnumbers[i+3] << "  ";
}

for(int i = 0; i < myproc.size(); i++){
    myproc[i].opriority = i;
}
std::sort(myproc.begin(),myproc.end(),process::arrivalfunctor());
cout << "\nThe (sorted) input is:  ";
cout << totnu << "   ";
for(int i = 0; i < myproc.size();i++){
    myproc[i].coutproc();
    cout << " ";
}
cout << "\n\n";
if(verbose == 1){
    cout << "This detailed printout gives the state and remaining burst for each process\n\n";
    cout << "Before cycle    "<< cycle << ":";
    for(int i = 0; i < myproc.size(); i++){

            cout << "\tunstarted  0";
    }
    cout << "\n";

}



for(int i =0; i < myproc.size(); i++){
    myproc[i].priority = i;
       if(myproc[i].arrivet != 0 ){
        notstart.push_back(myproc[i]);
    }else{
        rdy.push_back(myproc[i]);
    }
                 }
while(finishproc < myproc.size()){

    cycle++;




    if(curproc == NULL && !rdy.empty() ){

            theproc = rdy.front();
            curproc = &theproc;

            rdy.pop_front();
         if(curproc != NULL ){
            (*curproc).cpuburst = randomOS((*curproc).cburst);
         }
    }
      if(!rdy.empty()){
            for(int i = 0; i < rdy.size();i++){
                rdy[i].waittime++;
            }
        }

        for(int i =notstart.size() - 1; i >= 0 ; i--){
        if(notstart[i].arrivet  == cycle ){
            rdy.push_front(notstart[i]);
        }
    }


if(verbose == 1){
     cout << "Before cycle    "<< cycle << ":";
    for(int i = 0; i < myproc.size(); i++){
            if(std::find(block.begin(),block.end(),myproc[i]) != block.end()){
                    int bpos = find(block.begin(),block.end(),myproc[i]) - block.begin();
                    cout << "\tblocked  " << block[bpos].ioburst  ;

            }else if(std::find(rdy.begin(),rdy.end(),myproc[i]) != rdy.end()){
                    cout << "\tready    0";


            }else if (std::find(finished.begin(),finished.end(),myproc[i]) != finished.end()){
                    cout <<"\tterminated  0";
            }else if (std::find(notstart.begin(),notstart.end(),myproc[i]) != notstart.end() && cycle <= myproc[i].arrivet){
                cout <<"\tunstarted  0";
            }else{
               cout << "\trunning  " << ((*curproc).cpuburst);
            }

    }
    cout << "\n";

}
    if( curproc){

        (*curproc).remaint--;
        (*curproc).cpuburst--;

        CPUtime++;
    }




    vector<process> curbackh;
    if(curproc != NULL){

            if((*curproc).remaint == 0){
                (*curproc).finish = cycle;
                finished.push_back(*curproc);

                finishproc ++;


            }else if((*curproc).remaint != 0 && (*curproc).cpuburst <= 0){



                (*curproc).ioburst = randomOS((*curproc).oburst);


                curbackh.push_back(*curproc);


            }

          }


           vector<process> atrlist;
         vector<process>& bbuffer = block;
         vector<process> erasin;
     if(block.empty() != true){ // someone is blocked


            iotime++;
            int addtoready = 0;



        for(int i =  bbuffer.size() - 1; i >= 0; i--){

            bbuffer[i].ioburst--;
            bbuffer[i].iotime++;

            if(bbuffer[i].ioburst == 0){

                addtoready ++;


                atrlist.push_back(bbuffer[i]);

              erasin.push_back(bbuffer[i]);



            }
        }

     }









if(!block.empty()){


if(atrlist.size() == 1 ){



            rdy.push_front(atrlist[0]);




        }else if(atrlist.size() > 1){




















    sort(atrlist.begin(),atrlist.end(),process::antipriorityfunctor());

        for(int i = 0; i < atrlist.size(); i ++){
                rdy.push_front(atrlist[i]);
        }



        }

vector <process> blockh;
       for(int i = 0; i < block.size(); i++){
        bool boosa = false;
  for (int j = 0; j < erasin.size(); j ++){
        if(block[i] == erasin[j]){
            boosa = true;
        }
}
  if(boosa == false){
    blockh.push_back(block[i]);
   }
}
    block = blockh;


     }


      if(!curbackh.empty() && (*curproc).remaint != 0 && (*curproc).cpuburst <= 0 ){//&& (*curproc).remaint == 0){

block.push_back(curbackh[0]);
}







    if(curproc != NULL){

            if((*curproc).remaint == 0){

              curproc = NULL;
            }else if((*curproc).remaint != 0 && (*curproc).cpuburst <= 0){


              curproc = NULL;
            }

          }

}


cout << "The scheduling algorithm used was LastCome First Served\n\n";
double tturnaroundtime = 0;
double twaittime = 0;
std::sort(finished.begin(),finished.end(),process::priorityfunctor());
for(int i = 0; i < finished.size();i++){

    finished[i].turntime = finished[i].finish - finished[i].arrivet;
    tturnaroundtime += finished[i].turntime;
    twaittime += finished[i].waittime;
    cout << "Process "<<finished[i].priority << ":\n";
    cout << "\t(A,B,C,IO) = ("<<finished[i].arrivet <<","<< finished[i].cburst <<","<< finished[i].totcputime << ","<<finished[i].oburst << ")\n";
    cout << "\tFinishing time: "<<finished[i].finish << "\n";
    cout << "\tTurnaround time: "<<finished[i].turntime << "\n";
    cout << "\tI/O time: "<<finished[i].iotime << "\n";
    cout << "\tWaiting time: "<<finished[i].waittime << "\n\n";

}


std::streamsize defau = std::cout.precision();
cout << "Summary Data:\n";
cout << "\tFinishing time: " << cycle << "\n";
cout <<  "\tCPU Utilization: " << ((float)CPUtime/(float)cycle) << "\n";
cout << "\tI/O Utilization: " << (float)iotime/(float)cycle << "\n";
std::cout.precision(7);
cout <<"\tThroughput: " <<((double)(finished.size()) * (double)100.0)/(float)cycle  << " processes per hundred cycles"<<"\n";

cout <<  "\tAverage turnaround time: " << (double)((double) (tturnaroundtime)/(double)(finished.size())) << "\n";
cout << "\tAverage waiting time: " << (double) twaittime/(double)finished.size() << "\n";
curproc = NULL;
rcount = 0;
}

void RR2(vector <process> myproc, int verbose){
int cycle = 0;
int CPUtime = 0;
int iotime = 0;
int finishproc = 0;
int quantum = 2;
process *curproc = NULL;
process aproc(-1,-1,-1,-1);



deque<process> rdy;
vector<process> block;
vector<process> finished;
vector<process> notstart;
vector<process> tied;

cout << "The original input was: " << totnu << "   ";

for(int i = 1; i < pnumbers.size() - 1; i = i+4){
 cout << pnumbers[i] << " ";
 cout << pnumbers[i+1] << " ";
 cout << pnumbers[i+2] << " ";
 cout << pnumbers[i+3] << "  ";
}
cout << "\nThe (sorted) input is:  ";

for(int i = 0; i < myproc.size(); i++){
    myproc[i].opriority = i;
}
std::sort(myproc.begin(),myproc.end(),process::arrivalfunctor());
cout << totnu << "   ";
for(int i = 0; i < myproc.size();i++){
    myproc[i].coutproc();
    cout << " ";
}
cout << "\n\n";
if(verbose == 1){
    cout << "This detailed printout gives the state and remaining burst for each process\n\n";
    cout << "Before cycle    "<< cycle << ":";
    for(int i = 0; i < myproc.size(); i++){

            cout << "\tunstarted  0";
    }
    cout << "\n";

}



for(int i =0; i < myproc.size(); i++){
    myproc[i].priority = i;

    if(myproc[i].arrivet != 0 ){
        notstart.push_back(myproc[i]);
    }else{
        rdy.push_back(myproc[i]);
    }
                 }

while(finishproc < myproc.size()){
    for(int i =0; i < notstart.size(); i++){
        if(notstart[i].arrivet == cycle){
            rdy.push_back(notstart[i]);
        }
    }

    cycle++;


    if(curproc == NULL && !rdy.empty()){

            aproc = rdy.front();
            curproc = &aproc;
            rdy.pop_front();
         if(curproc != NULL && (*curproc).cpuburst <= 0 ){
                (*curproc).cpuburst = randomOS((*curproc).cburst);

         }
    }





if(verbose == 1){
     cout << "Before cycle    "<< cycle << ":";
    for(int i = 0; i < myproc.size(); i++){
            if(std::find(block.begin(),block.end(),myproc[i]) != block.end()){
                    int bpos = find(block.begin(),block.end(),myproc[i]) - block.begin();

                    cout << "\tblocked  " << block[bpos].ioburst  ;

            }else if(std::find(rdy.begin(),rdy.end(),myproc[i]) != rdy.end()){
                    cout << "\tready    0";


            }else if (std::find(finished.begin(),finished.end(),myproc[i]) != finished.end()){
                    cout <<"\tterminated  0";
            }else if (std::find(notstart.begin(),notstart.end(),myproc[i]) != notstart.end() && cycle <= myproc[i].arrivet){
                cout <<"\tunstarted  0";
            }else{
               cout << "\trunning  " << min(((*curproc).cpuburst),quantum);


            }


    }
    cout << "\n";

}

     quantum--;
    if(curproc != NULL){

        (*curproc).remaint--;
        (*curproc).cpuburst--;
        CPUtime++;


    }

        if(!rdy.empty()){
            for(int i = 0; i < rdy.size();i++){
                rdy[i].waittime++;


            }
        }




     if(block.empty() != true){ // someone is blocked

            iotime++;
           int addtoready = 0;
         vector<process> bready;
         vector<process> &bbuffer = block;
         vector<process> erasin;



        for(int i = 0; i < bbuffer.size(); i++){

            bbuffer[i].ioburst--;
            bbuffer[i].iotime++;

            if(bbuffer[i].ioburst <= 0){
                addtoready ++;
                bready.push_back(bbuffer[i]);
                erasin.push_back(bbuffer[i]);


            }
        }



        if(bready.size() == 1){
            tied.push_back(bready[0]);
        }else if (addtoready > 1){

               for(int i = 0; i < bready.size() - 1; i++){

                int minima = i;
            for(int j = i + 1; j < bready.size(); j++){
                if(bready[j].priority < bready[minima].priority){
                    minima = j;
                }
            }
          if(minima != i){
            process temp5 = bready[i];
            bready[i] = bready[minima];
            bready[minima] = temp5;
          }


          }

for(int i = 0; i < bready.size(); i ++){
    tied.push_back(bready[i]);
}

        }


vector <process> blockh;
       for(int i = 0; i < block.size(); i++){
        bool boosa = false;
  for (int j = 0; j < erasin.size(); j ++){
        if(block[i] == erasin[j]){
            boosa = true;
        }
}
  if(boosa == false){
    blockh.push_back(block[i]);
   }
}
    block = blockh;

     }

     if(quantum <= 0){


          if(curproc != NULL){
            if((*curproc).remaint == 0){
                (*curproc).finish = cycle;
                finished.push_back(*curproc);


                finishproc ++;


            }else if((*curproc).remaint != 0 && (*curproc).cpuburst <= 0){



                (*curproc).ioburst = randomOS((*curproc).oburst);
                block.push_back(*curproc);


            }else{

                tied.push_back(*curproc);
            }

            curproc = NULL;
            quantum = 2;
          }
       }   else{
              if(curproc != NULL){
              if((*curproc).remaint == 0){
                (*curproc).finish = cycle;
                finished.push_back(*curproc);


                finishproc ++;
                curproc = NULL;
                quantum = 2;

            }else if((*curproc).remaint != 0 && (*curproc).cpuburst <= 0){



                (*curproc).ioburst = randomOS((*curproc).oburst);
                block.push_back(*curproc);
                quantum = 2;
                curproc = NULL;
            }

              }else{
              quantum = 2;

              }

          }

          if(tied.size() != 1 && !tied.empty()){


          std::sort(tied.begin(),tied.end(),process::priorityfunctor());

              for(int i = 0; i < tied.size() - 1; i++){

                int minima = i;
            for(int j = i + 1; j < tied.size(); j++){
                if(tied[j].priority < tied[i].priority){
                    minima = j;
                }
            }
          if(minima != i){
            process temp4 = tied[i];
            tied[i] = tied[minima];
            tied[minima] = temp4;
          }


          }

          }

        for(int i = 0; i < tied.size(); i ++){

            rdy.push_back(tied[i]);
        }

          tied.clear();
 // loop end


}

cout << "The scheduling algorithm used was Round Robbin\n\n";
double tturnaroundtime = 0;
double twaittime = 0;
std::sort(finished.begin(),finished.end(),process::priorityfunctor());
for(int i = 0; i < finished.size();i++){

    finished[i].turntime = finished[i].finish - finished[i].arrivet;
    tturnaroundtime += finished[i].turntime;
    twaittime += finished[i].waittime;
    cout << "Process "<<finished[i].priority << ":\n";
    cout << "\t(A,B,C,IO) = ("<<finished[i].arrivet <<","<< finished[i].cburst <<","<< finished[i].totcputime << ","<<finished[i].oburst << ")\n";
    cout << "\tFinishing time: "<<finished[i].finish << "\n";
    cout << "\tTurnaround time: "<<finished[i].turntime << "\n";
    cout << "\tI/O time: "<<finished[i].iotime << "\n";
    cout << "\tWaiting time: "<<finished[i].waittime << "\n\n";

}


std::streamsize defau = std::cout.precision();
cout << "Summary Data:\n";
cout << "\tFinishing time: " << cycle << "\n";
cout <<  "\tCPU Utilization: " << ((float)CPUtime/(float)cycle) << "\n";
cout << "\tI/O Utilization: " << (float)iotime/(float)cycle << "\n";
std::cout.precision(7);
cout <<"\tThroughput: " <<((double)(finished.size()) * (double)100.0)/(float)cycle  << " processes per hundred cycles"<<"\n";
cout << "\tAverage turnaround time: " << (double) tturnaroundtime/(float)finished.size() << "\n";
cout << "\tAverage waiting time: " << (double) twaittime/(double)finished.size() << "\n";
curproc = 0;
rcount = NULL;


}

void HPRN(vector<process> myproc, int verbose){
int cycle = 0;
int CPUtime = 0;
int iotime = 0;
int finishproc = 0;
process *curproc = NULL;

process theproc(-1,-1,-1,-1);
for(int i = 0; i < plist.size(); i ++){
    myproc[i] = plist[i];
}

deque<process> rdy;
vector<process> block;
vector<process> finished;
vector<process> notstart;

cout << "The original input was: " << totnu << "   ";

for(int i = 1; i < pnumbers.size() - 1; i = i+4){
 cout << pnumbers[i] << " ";
 cout << pnumbers[i+1] << " ";
 cout << pnumbers[i+2] << " ";
 cout << pnumbers[i+3] << "  ";
}
cout << "\nThe (sorted) input is:  ";
cout << totnu << "   ";

for(int i = 0; i < myproc.size(); i++){
    myproc[i].opriority = i;
}
std::sort(myproc.begin(),myproc.end(),process::arrivalfunctor());
for(int i = 0; i < myproc.size();i++){
    myproc[i].coutproc();
    cout << " ";
}
cout << "\n\n";
if(verbose == 1){
    cout << "This detailed printout gives the state and remaining burst for each process\n\n";
    cout << "Before cycle    "<< cycle << ":";
    for(int i = 0; i < myproc.size(); i++){

              cout << "\tunstarted  0";
    }
    cout << "\n";

}



for(int i =0; i < myproc.size(); i++){
    myproc[i].priority = i;

    if(myproc[i].arrivet == 0 ){
            rdy.push_back(myproc[i]);
    }else{
        notstart.push_back(myproc[i]);
    }
                 }



while(finishproc < myproc.size()){

    for(int i =0; i < notstart.size(); i++){
        if(notstart[i].arrivet == cycle){
            notstart[i].ncycle = cycle;
            rdy.push_back(notstart[i]);
        }
    }

    cycle++;

    if(curproc == NULL ){

            if(!rdy.empty()){
            theproc = rdy.front();
            curproc = &theproc;

            rdy.pop_front();
            }
         if(curproc != NULL ){
            (*curproc).cpuburst = randomOS((*curproc).cburst);
         }
    }



if(verbose == 1){
     cout << "Before cycle    "<< cycle << ":";
    for(int i = 0; i < myproc.size(); i++){
            if(std::find(block.begin(),block.end(),myproc[i]) != block.end()){
                    int bpos = find(block.begin(),block.end(),myproc[i]) - block.begin();

                    cout << "\tblocked  " << block[bpos].ioburst  ;

            }else if(std::find(rdy.begin(),rdy.end(),myproc[i]) != rdy.end()){
                    cout << "\tready    0";


            }else if (std::find(finished.begin(),finished.end(),myproc[i]) != finished.end()){
                    cout <<"\tterminated  0";
            }else if (std::find(notstart.begin(),notstart.end(),myproc[i]) != notstart.end() && cycle <= myproc[i].arrivet){
                cout <<"\tunstarted  0";
            }else{
               cout << "\trunning  " << ((*curproc).cpuburst);


            }


    }
    cout << "\n";

}

    if(curproc != NULL){

        (*curproc).remaint--;
        (*curproc).cpuburst--;
        CPUtime++;
        (*curproc).ncycle = cycle;

    }

        if(!rdy.empty()){
            for(int i = 0; i < rdy.size();i++){
                rdy[i].waittime++;
                rdy[i].ncycle = cycle;

            }
        }




     if(block.empty() != true){ // someone is blocked

            iotime++;

         vector<process> erasin;
         vector<process> bready;
         vector<process>& bbuffer = block;





        for(int i = 0; i < bbuffer.size(); i++){

            bbuffer[i].ioburst--;
            bbuffer[i].iotime++;
            bbuffer[i].ncycle = cycle;

            if(bbuffer[i].ioburst == 0){

                bready.push_back(bbuffer[i]);

               erasin.push_back(bbuffer[i]);

            }

        }


        for(int i = 0; i < bready.size();i++){
            rdy.push_back(bready[i]);
        }

vector <process> blockh;
       for(int i = 0; i < block.size(); i++){
        bool boosa = false;
  for (int j = 0; j < erasin.size(); j ++){
        if(block[i] == erasin[j]){
            boosa = true;
        }
}
  if(boosa == false){
    blockh.push_back(block[i]);
   }
}
    block = blockh;


     }



          if(curproc != NULL){
            if((*curproc).remaint == 0){
                (*curproc).finish = cycle;
                finished.push_back(*curproc);


                finishproc ++;
                curproc = NULL;

            }else if((*curproc).remaint != 0 && (*curproc).cpuburst <= 0){



                (*curproc).ioburst = randomOS((*curproc).oburst);
                block.push_back(*curproc);

                curproc = NULL;
            }
          }

        if(!rdy.empty()){

            vector<process> tmp;





            for(int i = 0; i < rdy.size();  i++){
                tmp.push_back(rdy[i]);
            }
            rdy.clear();

sort(tmp.begin(),tmp.end(),process::hprnfunctor());









        for(int i = 0; i < tmp.size(); i ++){
            rdy.push_back(tmp[i]);

        }



        }


} // loop end

cout << "The scheduling algorithm used was Highest Penalty Ratio Next\n\n";
double tturnaroundtime = 0;
double twaittime = 0;
std::sort(finished.begin(),finished.end(),process::priorityfunctor());
for(int i = 0; i < finished.size();i++){

    finished[i].turntime = finished[i].finish - finished[i].arrivet;
    tturnaroundtime += finished[i].turntime;
    twaittime += finished[i].waittime;
    cout << "Process "<<finished[i].priority << ":\n";
    cout << "\t(A,B,C,IO) = ("<<finished[i].arrivet <<","<< finished[i].cburst <<","<< finished[i].totcputime << ","<<finished[i].oburst << ")\n";
    cout << "\tFinishing time: "<<finished[i].finish << "\n";
    cout << "\tTurnaround time: "<<finished[i].turntime << "\n";
    cout << "\tI/O time: "<<finished[i].iotime << "\n";
    cout << "\tWaiting time: "<<finished[i].waittime << "\n\n";

}


std::streamsize defau = std::cout.precision();
cout << "Summary Data:\n";
cout << "\tFinishing time: " << cycle << "\n";
cout <<  "\tCPU Utilization: " << ((float)CPUtime/(float)cycle) << "\n";
cout << "\tI/O Utilization: " << (float)iotime/(float)cycle << "\n";
std::cout.precision(7);
cout <<"\tThroughput: " <<((double)(finished.size()) * (double)100.0)/(float)cycle  << " processes per hundred cycles"<<"\n";
cout << "\tAverage turnaround time: " << (double) tturnaroundtime/(float)finished.size() << "\n";
cout << "\tAverage waiting time: " << (double) twaittime/(double)finished.size() << "\n";
rcount = 0;
curproc = NULL;
}

void FCFS(vector<process> myproc, int verbose){
int cycle = 0;
int CPUtime = 0;
int iotime = 0;
int finishproc = 0;

process *curproc = NULL;
process theproc(-1,-1,-1,-1);


deque<process> rdy;
vector<process> block;
vector<process> finished;
vector<process> notstart;
cout << "The original input was: " << totnu << "   ";

for(int i = 1; i < pnumbers.size() - 1; i = i+4){
 cout << pnumbers[i] << " ";
 cout << pnumbers[i+1] << " ";
 cout << pnumbers[i+2] << " ";
 cout << pnumbers[i+3] << "  ";
}

for(int i = 0; i < myproc.size(); i++){
    myproc[i].opriority = i;
}
std::sort(myproc.begin(),myproc.end(),process::arrivalfunctor());
cout << "\nThe (sorted) input is:  ";
cout << totnu << "   ";
for(int i = 0; i < myproc.size();i++){
    myproc[i].coutproc();
    cout << " ";
}
cout << "\n\n";
if(verbose == 1){
    cout << "This detailed printout gives the state and remaining burst for each process\n\n";
    cout << "Before cycle    "<< cycle << ":";
    for(int i = 0; i < myproc.size(); i++){

              cout << "\tunstarted  0";
    }
    cout << "\n";

}





for(int i =0; i < myproc.size(); i++){
    myproc[i].priority = i;

    if(myproc[i].arrivet != 0 ){
        notstart.push_back(myproc[i]);
    }else{
        rdy.push_back(myproc[i]);
    }
                 }

while(finishproc < myproc.size()){

    for(int i =0; i < notstart.size(); i++){
        if(notstart[i].arrivet == cycle){
            rdy.push_back(notstart[i]);
        }
    }

    cycle++;

    if(!curproc&& !rdy.empty()){

            theproc = rdy.front();
            curproc = &theproc;

            rdy.pop_front();
         if(curproc  ){
            (*curproc).cpuburst = randomOS((*curproc).cburst);

         }
    }



if(verbose == 1){
     cout << "Before cycle    "<< cycle << ":";
    for(int i = 0; i < myproc.size(); i++){
            if(std::find(block.begin(),block.end(),myproc[i]) != block.end()){
                    int bpos = find(block.begin(),block.end(),myproc[i]) - block.begin();

                    cout << "\tblocked  " << block[bpos].ioburst  ;

            }else if(std::find(rdy.begin(),rdy.end(),myproc[i]) != rdy.end()){
                    cout << "\tready    0";


            }else if (std::find(finished.begin(),finished.end(),myproc[i]) != finished.end()){
                    cout <<"\tterminated  0";
            }else if (std::find(notstart.begin(),notstart.end(),myproc[i]) != notstart.end() && cycle <= myproc[i].arrivet){
                cout <<"\tunstarted  0";
            }else{
               cout << "\trunning  " << ((*curproc).cpuburst);


            }


    }
    cout << "\n";

}

    if( curproc){

        (*curproc).remaint--;
        (*curproc).cpuburst--;

        CPUtime++;
    }
        if(!rdy.empty()){
            for(int i = 0; i < rdy.size();i++){
                rdy[i].waittime++;

            }
        }




     if(block.empty() != true){ // someone is blocked

        vector<process> erasin;
            iotime++;
            int addtoready = 0;
         vector<process> bready;
         vector<process>& bbuffer = block;



        for(int i = 0; i < bbuffer.size(); i++){

            bbuffer[i].ioburst--;
            bbuffer[i].iotime++;

            if(bbuffer[i].ioburst == 0){

                addtoready ++;

                bready.push_back(bbuffer[i]);

              erasin.push_back(bbuffer[i]);



            }
        }





if(bready.size() == 1 ){
            rdy.push_back(bready[0]);


        }else if(addtoready > 1){


               for(int i = 0; i < bready.size() - 1; i++){

                int minima = i;
            for(int j = i + 1; j < bready.size(); j++){
                if(bready[j].priority < bready[i].priority){
                    minima = j;
                }
            }
          if(minima != i){
            process temp = bready[i];
            bready[i] = bready[minima];
            bready[minima] = temp;
          }


          }


        for(int i = 0; i < bready.size(); i ++){
            rdy.push_back(bready[i]);
        }

        }



vector <process> blockh;
       for(int i = 0; i < block.size(); i++){
        bool boosa = false;
  for (int j = 0; j < erasin.size(); j ++){
        if(block[i] == erasin[j]){
            boosa = true;
        }
}
  if(boosa == false){
    blockh.push_back(block[i]);
   }
}
    block = blockh;


     }




          if(curproc != NULL){

            if((*curproc).remaint == 0){
                (*curproc).finish = cycle;
                finished.push_back(*curproc);

                finishproc ++;
                curproc = NULL;

            }else if((*curproc).remaint != 0 && (*curproc).cpuburst <= 0){



                (*curproc).ioburst = randomOS((*curproc).oburst);


                block.push_back(*curproc);

                curproc = NULL;
            }

          }


}

cout << "The scheduling algorithm used was First Come First Served\n\n";
double tturnaroundtime = 0;
double twaittime = 0;
std::sort(finished.begin(),finished.end(),process::priorityfunctor());
for(int i = 0; i < finished.size();i++){

    finished[i].turntime = finished[i].finish - finished[i].arrivet;
    tturnaroundtime += finished[i].turntime;
    twaittime += finished[i].waittime;
    cout << "Process "<<finished[i].priority << ":\n";
    cout << "\t(A,B,C,IO) = ("<<finished[i].arrivet <<","<< finished[i].cburst <<","<< finished[i].totcputime << ","<<finished[i].oburst << ")\n";
    cout << "\tFinishing time: "<<finished[i].finish << "\n";
    cout << "\tTurnaround time: "<<finished[i].turntime << "\n";
    cout << "\tI/O time: "<<finished[i].iotime << "\n";
    cout << "\tWaiting time: "<<finished[i].waittime << "\n\n";

}


std::streamsize defau = std::cout.precision();
cout << "Summary Data:\n";
cout << "\tFinishing time: " << cycle << "\n";
cout <<  "\tCPU Utilization: " << ((float)CPUtime/(float)cycle) << "\n";
cout << "\tI/O Utilization: " << (float)iotime/(float)cycle << "\n";
std::cout.precision(7);
cout <<"\tThroughput: " <<((double)(finished.size()) * (double)100.0)/(float)cycle  << " processes per hundred cycles"<<"\n";
cout << "\tAverage turnaround time: " << (double) tturnaroundtime/(float)finished.size() << "\n";
cout << "\tAverage waiting time: " << (double) twaittime/(double)finished.size() << "\n";
curproc = NULL;
rcount = 0;
}


int randomOS(int U){
int randn = numba[rcount++];
                 return 1 + (randn%U);
}


vector<process> rearrange(vector<process>* inproc){

    for(int i=0; i < (*inproc).size() - 1; i++){
            int min = i;
            process * holdi = &((*inproc)[i]);
            process * holdj;
        for(int j = i+1; j < (*inproc).size();j++){

            if((*inproc)[j].arrivet < (*inproc)[i].arrivet ){
                min = j;

                }
                holdj = &((*inproc)[min]);
        }
    if(min != i ){


        process hold = (*inproc)[i];
        *holdi = (*inproc)[min];
        *holdj = hold;

    }

    }

}

   bool floatequal(double a1, double a2){
    return fabs(a1 -a2) < 0.00000001;
    }



